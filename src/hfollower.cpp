#include <hfollower/hfollower.hpp>

using namespace std;
using namespace std_msgs;

/**
 * A node which gets position from humans detected
 * and choose the optimal one to follow, in accordance
 * with a scoring system.
 * Human position in laser frame
 * It is supposed that once an Human with a determined
 * ID is gone, this ID will not appear any more
 * @author Andrés Alacid Cano
 * @date 2014/02/15
 */

//HumanFollower::HumanFollower(): ac("/Summit_UPM/move_base", true)
HumanFollower::HumanFollower(): ac("/move_base", true)
{    
    // Frame used
//    frame = "/Summit_UPM/odom_combined";
    frame = "/odom";

    // Weight scoring variables
    k1 = 1;
    k2 = 1.5;
    k3 = 1.1;

    // Scores
    sc1 = 0.0;
    sc2 = 0.0;
    sc3 = 0.0;
    scoreaux = 0.0;

    lastupdateduration = 0.0;
    aux = 0.0;

    // Initialize variables
    idMaxSc = 0;
    firstData = false;
    firstFollow = false;
    message_counter = 0;
    reached = false;
    goal_sent = false;
    goal_frame_on = false;
    with_amcl = false;
    with_odom_combined = false;
    x2 = 0;
    y2 = 0;

    // Robot AMCL position subscription callback
    amcl_position = nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>("/Summit_UPM/ammcl_pose", 1, &HumanFollower::amcl_subCallback, this);

    // Robot ODOM position subscription callback
    odom_sub = nh.subscribe<nav_msgs::Odometry>("/odom", 1, &HumanFollower::odom_Callback, this);


    // Robot EKF ODOM COMBINED position subscription callback
    odom_combined_sub = nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>("/ekf_pose/odom_combined", 
                                                                    1, &HumanFollower::odom_combinedCallback, this);


    // Humans detected subscription callback
    humans_detected_sub = nh.subscribe<hdetect::HumansFeatClass>("/HumansDetected", 1, &HumanFollower::humans_detectedCallback, this);

    // Manual turning setting
    cmd_vel_pub_ = nh.advertise<geometry_msgs::Twist>("/move_base/cmd_vel", 1);

    // Objective publisher
    Objective = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>("/HumanObjective", 10);

    /// DEBUGGING
    //obj_position_odom = nh.advertise<geometry_msgs::PointStamped>("/HumanPositionOdom", 1);
    //obj_position_laser = nh.advertise<geometry_msgs::PointStamped>("/HumanPositionLaser", 1);

    ROS_INFO("[HFOLLOWER] - subcribers ok!");


  // Wait for the action server to come up
    while(!ac.waitForServer(ros::Duration(5.0))){
        ROS_INFO("[HFOLLOWER] Waiting for the move_base action server to come up\n\n");
    }

    

    ROS_INFO("[HFOLLOWER] Follower running OK.");
}


// ROBOT POSITION CALLBACK AMCL
void HumanFollower::amcl_subCallback(const geometry_msgs::PoseWithCovarianceStamped amcl_robot1_position)
{

    with_amcl = true;

    x1amcl = amcl_robot1_position.pose.pose.position.x;
    y1amcl = amcl_robot1_position.pose.pose.position.y;
    z1amcl = amcl_robot1_position.pose.pose.orientation.z;
    w1amcl = amcl_robot1_position.pose.pose.orientation.w;

    yaw_robotamcl = tf::getYaw(amcl_robot1_position.pose.pose.orientation);
    while(yaw_robotamcl < 0) yaw_robotamcl += 2*M_PI;
    while(yaw_robotamcl > 2*M_PI) yaw_robotamcl -= 2*M_PI;
}


// ROBOT POSITION CALLBACK ODOM_COMBINED
void HumanFollower::odom_combinedCallback(const geometry_msgs::PoseWithCovarianceStamped odom_combined_robot)
{

    with_odom_combined = true;

    x1odom = odom_combined_robot.pose.pose.position.x;
    y1odom = odom_combined_robot.pose.pose.position.y;
    z1odom = odom_combined_robot.pose.pose.orientation.z;
    w1odom = odom_combined_robot.pose.pose.orientation.w;

    yaw_robotodom = tf::getYaw(odom_combined_robot.pose.pose.orientation);
    while(yaw_robotodom < 0) yaw_robotodom += 2*M_PI;
    while(yaw_robotodom > 2*M_PI) yaw_robotodom -= 2*M_PI;
}

// ROBOT POSITION CALLBACK ODOM_COMBINED
void HumanFollower::odom_Callback(const nav_msgs::Odometry odom_robot)
{
    with_odom = true;

    x1odom = odom_robot.pose.pose.position.x;
    y1odom = odom_robot.pose.pose.position.y;
    z1odom = odom_robot.pose.pose.orientation.z;
    w1odom = odom_robot.pose.pose.orientation.w;

    yaw_robotodom = tf::getYaw(odom_robot.pose.pose.orientation);
    while(yaw_robotodom < 0) yaw_robotodom += 2*M_PI;
    while(yaw_robotodom > 2*M_PI) yaw_robotodom -= 2*M_PI;
}



// HUMANS DETECTED CALLBACK
void HumanFollower::humans_detectedCallback(const hdetect::HumansFeatClass HumansDetected)
{
    if (HumansDetected.HumansDetected.size() != 0)
    {
      for (uint i=0 ; i < HumansDetected.HumansDetected.size() ; i++)
      {

            // Detection time scoring. Logarithmic function
            detectionDuration = (ros::Time::now().toSec() - HumansDetected.HumansDetected.at(i).detectiontime);
	    //ROS_INFO("Hum Id %d, dectection duration %f",HumansDetected.HumansDetected.at(i).id, detectionDuration);
            sc1 = 36.06 * log(0.1333 * (detectionDuration + 7.50187));
	    
	    double dist_hu;

	    dist_hu = (HumansDetected.HumansDetected.at(i).x-x1odom)*(HumansDetected.HumansDetected.at(i).x-x1odom) +
		  (HumansDetected.HumansDetected.at(i).y-y1odom)*(HumansDetected.HumansDetected.at(i).y-y1odom);
            dist_hu =  sqrt(dist_hu);
	    //ROS_INFO("Distance to human %f", dist_hu);
            // Distance scoring. Lineal function
            if(dist_hu < 0.3)
            {
              sc2 = 0;
            }
            else
            {
                sc2 = -9.5238*dist_hu + 114.28;
            }

            // Following time scoring. Logarithmic function
            if (HumansDetected.HumansDetected.at(i).id == HumanObjective.idStruct)
            {
                followingDuration = (ros::Time::now().toNSec() - HumanObjective.followingtimeStruct);
                //ROS_INFO("Hum Id %d, following Duration %f",HumansDetected.HumansDetected.at(i).id, followingDuration);
                sc3 = 66.06 * log(0.1333 * (followingDuration + 7.50187));
            }

            // Total scoring
            scoreaux = k1 * sc1 + k2 * sc2 + k3 * sc3;
            //ROS_INFO("Hum Id: %d, Score: %f", HumansDetected.HumansDetected.at(i).id, scoreaux);

            if (scoreaux > AuxStruct.scoreStruct)
            {
                AuxStruct.idStruct = HumansDetected.HumansDetected.at(i).id;
                AuxStruct.scoreStruct = scoreaux;
              //  ROS_INFO("Best Hum Id: %d, Score: %f", HumansDetected.HumansDetected.at(i).id, scoreaux);
            }
        }

        for(uint i=0 ; i < HumansDetected.HumansDetected.size() ; i++)
        {

            if (HumansDetected.HumansDetected.at(i).id == AuxStruct.idStruct)
            {
                //ROS_INFO("Hum to be followed Id: %d, Pos(%f, %f) Score: %f", AuxStruct.idStruct,
		//	HumansDetected.HumansDetected.at(i).x,  HumansDetected.HumansDetected.at(i).y, AuxStruct.scoreStruct);
                HumanObjective.xStruct = HumansDetected.HumansDetected.at(i).x;
                HumanObjective.yStruct = HumansDetected.HumansDetected.at(i).y;
                HumanObjective.distanceStruct = (HumansDetected.HumansDetected.at(i).x*HumansDetected.HumansDetected.at(i).x +
                    HumansDetected.HumansDetected.at(i).y*HumansDetected.HumansDetected.at(i).y);
                HumanObjective.detectiontimeStruct = HumansDetected.HumansDetected.at(i).detectiontime;
                HumanObjective.scoreStruct = AuxStruct.scoreStruct;
                if (AuxStruct.idStruct != HumanObjective.idStruct)
                {
                    HumanObjective.followingtimeStruct = ros::Time::now().toSec();
                    HumanObjective.idStruct = AuxStruct.idStruct;
                }
            }
        }

        // Objective publisher
        ObjectiveStamped.header.seq = HumanObjective.idStruct;
        ObjectiveStamped.header.stamp = ros::Time::now();
        ObjectiveStamped.header.frame_id = frame;
        ObjectiveStamped.pose.pose.position.x = HumanObjective.xStruct;
        ObjectiveStamped.pose.pose.position.y = HumanObjective.yStruct;
        Objective.publish(ObjectiveStamped);

        // Reset auxiliar variable
        AuxStruct.idStruct = 0;
        AuxStruct.scoreStruct = -100; // Guarantee that it is going to be always smaller than new score

        // New objective position assignment
        xlaser = HumanObjective.xStruct;
        ylaser = HumanObjective.yStruct;        
	//ROS_INFO("xlaser %f  -  ylaser %f", xlaser, ylaser);

        decisionfollower();
    }
}


// DECISION-MAKING FUNCTION
void HumanFollower::decisionfollower()
{  

//    // Part of the code useful when following node is separated
//    ros::Rate rate(1.0);
//    while (ros::ok() && firstData) {

    // Robot position from appropiate frame
    if (with_amcl)
    {
        x1 = x1amcl;
        y1 = y1amcl;
        z1 = z1amcl;
        w1 = w1amcl;
        yaw_robot = yaw_robotamcl;
    }
    else if(with_odom_combined)
    {
        x1 = x1odom;
        y1 = y1odom;
        z1 = z1odom;
        w1 = w1odom;
        yaw_robot = yaw_robotodom;
    }
    else if(with_odom)
    {
        x1 = x1odom;
        y1 = y1odom;
        z1 = z1odom;
        w1 = w1odom;
        yaw_robot = yaw_robotodom;
    }


    // Objective position initialization with first data acquisition
    if (!firstData) {
        x2 = xlaser;// - x1;
        y2 = ylaser;// - y1;
        // Rotation to /odom_combined axes
      //  x2rot = x2 * cos(yaw_robot) - y2 * sin(yaw_robot);
      //  y2rot = x2 * sin(yaw_robot) + y2 * cos(yaw_robot);
      //  x2odom = x2;// + x2rot;
      //  y2odom = y2;// + y2rot;
        firstData = true;
    }

    // Transformation from /laser_top frame to /odom_combined frame
    // Rotation matrix and movement
  //  xodom = x1 + xlaser * cos(yaw_robot) - ylaser * sin(yaw_robot);
  //  yodom = y1 + xlaser * sin(yaw_robot) + ylaser + cos(yaw_robot);

    // Objective distance from robot in axes
//    // This is used when human position is in /odom_combined frame
//      xlaser = xlaser - x1;
// /     ylaser = ylaser - y1

    //  ROS_INFO("xlaser %f - x1 %f -  x2 %f , ylaser %f - y1 %f - y2 %f", xlaser, x1, x2, ylaser, y1, y2);
    // Objective distance from robot
    obj_dist = sqrt((xlaser-x1)*(xlaser-x1) + (ylaser-y1)*(ylaser-y1));

    // Objective movement
//    // This is used when human position is in /odom_combined frame
      obj_mov = sqrt((x2 - xlaser)*(x2 - xlaser) + (y2 - ylaser)*(y2 - ylaser));
//    obj_mov = sqrt((x2odom - xodom)*(x2odom - xodom) + (y2odom - yodom)*(y2odom - yodom));

    // Information on screen
//    ROS_INFO(ANSI_COLOR_RED"  [HUMANFOLLOWERRT] OBJ DIST = %f   OBJ MOV = %f"ANSI_COLOR_RESET"", obj_dist, obj_mov);

    // First values assignment and following or framing start
    if (!firstFollow){

        if (obj_dist > 0.001) {
            ROS_INFO(ANSI_COLOR_RED"  [DECISION MAKING] Initial goal_follower calling"ANSI_COLOR_RESET"\n\n");
            goal_follower();
        }
        else {
            ROS_INFO(ANSI_COLOR_RED"  [DECISION MAKING] Initial goal_frame calling"ANSI_COLOR_RESET"\n\n");
            goal_frame();
        }
        firstFollow = true;
    }

    // Time check
    lastupdateduration = (ros::Time::now() - lastupdate).sec + (((ros::Time::now() - lastupdate).nsec) * 0.000000001);

    // Everytime objective moves a determined distance around the framing or the goal must be updated
    if((obj_mov > 1.5) || (lastupdateduration > 5)) {
    //if(lastupdateduration > 2.9) {
        lastupdate = ros::Time::now();

        // Update position
        x2 = xlaser;
        y2 = ylaser;
        // Rotation to /odom_combined axes
        //x2rot = x2 * cos(yaw_robot) - y2 * sin(yaw_robot);
        //y2rot = x2 * sin(yaw_robot) + y2 * cos(yaw_robot);
        //x2odom = x2;// + x2rot;
        //y2odom = y2;// + y2rot;

//    ROS_INFO(ANSI_COLOR_RED"  [DECISION MAKING] Objective moved %f meters around or time since last goal update is bigger than %f seconds. Distance to objective %f. Function called is:"ANSI_COLOR_RESET, obj_mov, lastupdateduration, obj_dist);
        goal_frame_on = false;
        reached = false;
        if (obj_dist > 0.001) {
//          ROS_INFO(ANSI_COLOR_RED"  [DECISION MAKING] goal_follower"ANSI_COLOR_RESET"\n\n");
            goal_follower();

        }
        else {
            ROS_INFO(ANSI_COLOR_RED"  [DECISION MAKING] goal_frame"ANSI_COLOR_RESET"\n\n");
            // Robot position updating
            // X2 and Y2 will be the real goal to follow, unless the robot moves one meter around again
            goal_frame();
        }
    }
    else if (obj_dist < 0.001 && goal_sent) {
        // Robot came into framing zone
        ROS_INFO(ANSI_COLOR_RED"    [DECISION MAKING] Goal frame function starts"ANSI_COLOR_RESET"\n\n");
        goal_frame();
    }
    // Frame funcition calling successively
    else if(goal_frame_on && !reached) {
            //rate.sleep();
            goal_frame();
    }

    /// DEBUGGING
//    ROS_INFO(ANSI_COLOR_RED"  [HUMANFOLLOWERRT] DECISION PART -> X2ODOM = %f"ANSI_COLOR_RESET"\n\n", x2odom);



//            ros::spinOnce();
//    }

}


// FOLLOWING FUNCTION
void HumanFollower::goal_follower()
{

    // Theta in frame /laser_top (Same frame as the goal frame)
    thetaObj = atan2(ylaser-y1,xlaser-x1);
    while(thetaObj < 0) thetaObj += 2*M_PI;
    while(thetaObj > 2*M_PI) thetaObj -= 2*M_PI;

    

    xgoal = xlaser - 0.7*cos(thetaObj);
    ygoal = ylaser - 0.7*sin(thetaObj);

    //Goal position one met1er and a half before the objective, in a line that comprises the objective and the robot. Goal in frame /odom_combined
//    xgoal = - ((3*x2odom-3*x1) * sqrt (y2odom*y2odom - 2*y1*y2odom + y1*y1 + x2odom*x2odom - 2*x1*x2odom + x1*x1) + 2*(- x2odom*y2odom*y2odom + 2*x2odom*y1*y2odom - x2odom*y1*y1 - x2odom*x2odom*x2odom + 2*x1*x2odom*x2odom - x1*x1*x2odom))  /  (2*(y2odom*y2odom - 2*y1*y2odom + y1*y1 + x2odom*x2odom - 2*x1*x2odom + x1*x1));
//    ygoal = - ((3*y2odom-3*y1) * sqrt (y2odom*y2odom - 2*y1*y2odom + y1*y1 + x2odom*x2odom - 2*x1*x2odom + x1*x1) + 2*(- y2odom*y2odom*y2odom + 2*y1*y2odom*y2odom + (- y1*y1 - x2odom*x2odom + 2*x1*x2odom - x1*x1) * y2odom))  /  (2*(y2odom*y2odom - 2*y1*y2odom + y1*y1 + x2odom*x2odom - 2*x1*x2odom + x1*x1));

    //Goal position one meter and a half before the objective, in a line that comprises the objective and the robot. Goal in frame /laser_top
  //  xgoal = - ((3*x2) * sqrt (y2*y2 + x2*x2) + 2*(- x2*y2*y2 - x2*x2*x2))  /  (2*(y2*y2 + x2*x2));
  //  ygoal = - ((3*y2) * sqrt (y2*y2 + x2*x2) + 2*(- y2*y2*y2 + (- x2*x2) * y2))  /  (2*(y2*y2 + x2*x2));

    // INDOOR
    // Goal position half a meter before the objective, in a line that comprises the objective and the robot. Goal in frame /odom_combined
    //x = - ((sqrt(2)*x2odom-sqrt(2)*x1) * sqrt (y2odom*y2odom - 2*y1*y2odom + y1*y1 + x2odom*x2odom - 2*x1*x2odom + x1*x1) + 2*(- x2odom*y2odom*y2odom + 2*x2odom*y1*y2odom - x2odom*y1*y1 - x2odom*x2odom*x2odom + 2*x1*x2odom*x2odom - x1*x1*x2odom))  /  (2*(y2odom*y2odom - 2*y1*y2odom + y1*y1 + x2odom*x2odom - 2*x1*x2odom + x1*x1));
    //y = - ((sqrt(2)*y2odom-sqrt(2)*y1) * sqrt (y2odom*y2odom - 2*y1*y2odom + y1*y1 + x2odom*x2odom - 2*x1*x2odom + x1*x1) + 2*(- y2odom*y2odom*y2odom + 2*y1*y2odom*y2odom + (- y1*y1 - x2odom*x2odom + 2*x1*x2odom - x1*x1) * y2odom))  /  (2*(y2odom*y2odom - 2*y1*y2odom + y1*y1 + x2odom*x2odom - 2*x1*x2odom + x1*x1));

//        /// DEBUGGING
//        geometry_msgs::PointStamped obj_pose_laser;
//        obj_pose_laser.header.frame_id = "/laser_top";
//        obj_pose_laser.header.stamp = ros::Time::now();
//        obj_pose_laser.point.x = xgoal;
//        obj_pose_laser.point.y = ygoal;
//        obj_position_laser.publish(obj_pose_laser);


    ///
    /// /// RECUERDA QUE ES EN EL FRAME DEL LASER! LOS YAW Y THETAS TIENES QUE REVISARLOS!!
    ///

    // Goal value assignment
    move_base_msgs::MoveBaseGoal goal;
    goal.target_pose.header.frame_id = frame;
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.pose.position.x = xgoal;
    goal.target_pose.pose.position.y = ygoal;
    goal.target_pose.pose.orientation.z = sin(thetaObj/2);
    goal.target_pose.pose.orientation.w = cos(thetaObj/2);
    ROS_INFO("[GOAL FOLLOWER] New goal: X: %f    Y: %f\n", xgoal, ygoal);
    ac.sendGoal(goal);
    goal_sent = true;

    /// DEBUGGING
    //ROS_INFO(ANSI_COLOR_RED"  [HUMANFOLLOWERRT] FOLLOWER PART -> X2ODOM = %f"ANSI_COLOR_RESET"\n\n", x2odom);

}


// FRAMING FUNCTION
// Turn speed is determined and the angle turned is checked out in order to find out when the final objective is framed
void HumanFollower::goal_frame()
{

    // First time it entered into the framing function
    if (!goal_frame_on) {

        // Boolean to indicate framing process
        goal_frame_on = true;

        // Starting Yaw
        //yaw_first = yaw_robot; // Commented because with the data on /laser_top frame, only information on
                                 // /laser_top yaw is needed


        // Angle between the robot position and the objective
        // Just from /laser_top data
	thetaObj = atan2(ylaser-y1,xlaser-x1);
        while(thetaObj < 0) thetaObj += 2*M_PI;
        while(thetaObj > 2*M_PI) thetaObj -= 2*M_PI;

        // Turn angle determined by the difference between robot angle and robot-objective angle
        //radians = yaw_robot - thetaObj;
        radians = thetaObj; // When data from objective comes from /laser_top frame

        // Check out of the angle, regarding that it is not bigger than 2*PI neither negative
        //while(radians < 0) radians += 2*M_PI;
        //while(radians > 2*M_PI) radians -= 2*M_PI;
        if (radians > 0 && radians < M_PI) {
            clockwise = true;
            radiansPI = radians;
            ROS_INFO("  Clockwise turn\n");
        }
        else {
            radiansPI = 2*M_PI - radians;
            clockwise = false;
            ROS_INFO("  Counterclockwise turn\n");
        }

        // Turn speed determination in rad/s. It will be proportional to the turn angle amount
        base_cmd.linear.x = base_cmd.linear.y = 0.0;
        base_cmd.angular.z = (1.5/M_PI) * radiansPI + 0.5;	// Radians is the turn angle between 0 and 180 degrees
        // Desired turn axis
        tf::Vector3 desired_turn_axis(0,0,1);

        // Turn direction
        if (clockwise) base_cmd.angular.z = -base_cmd.angular.z;

        // If it is already close to perfect framing (+- 5 degrees), not to do framing function
        if (radiansPI < 0.09) {
            reached = true;
            goal_frame_on = false; // Framing is over
            base_cmd.angular.z = 0;
            ROS_INFO("  Framing process succeeded\n\n");
        }

        ROS_INFO("  Turn to do: %f\n", radians);

    }

    // Unless it has entered in the framing function first, cancel the current goal
    if (goal_sent) {
        ac.cancelGoal();
        ROS_INFO("  Goal cancelled\n");
        if(ac.getState() == actionlib::SimpleClientGoalState::PREEMPTED) {  // ERROR. It does not do the "if". Status goes from active state to state 6, and instantly goes to state 2 (PREEMPTED)
            //ROS_INFO("  Goal cancelled\n\n");
            //goal_sent = false;
        }
        goal_sent = false;
    }


    // Turn angle determined by the difference between robot angle and robot-objective angle
    // Just from /laser_top data
    thetaObj = atan2(ylaser-y1,xlaser-x1);
    while(thetaObj < 0) thetaObj += 2*M_PI;
    while(thetaObj > 2*M_PI) thetaObj -= 2*M_PI;

    // Turn angle determined by the difference between robot angle and robot-objective angle
    //radians = yaw_robot - thetaObj;
    radians = thetaObj; // When data from objective comes from /laser_top frame

    // Check out of the angle, regarding that it is not bigger than 2*PI neither negative
   // while(radians < 0) radians += 2*M_PI;
   // while(radians > 2*M_PI) radians -= 2*M_PI;
    if (radians < M_PI)	radiansPI = radians;
    else radiansPI = 2*M_PI - radians;

    // Algorithm to show regularly the status on the screen
    if (message_counter == 0) ROS_INFO("  Radian angle to turn: %f\n", radiansPI);
    message_counter ++;
    if (message_counter == 1) message_counter = 0;

    // Check out if framing has arrived to objective
    if (clockwise && (radians > M_PI)){
        reached = true;
        goal_frame_on = false; // Framing is over
        // Manual turning stop
        base_cmd.angular.z = 0;
        cmd_vel_pub_.publish(base_cmd);
        ROS_INFO("  Encuadre realizado\n\n");
    }
    else if (!clockwise && (radians < M_PI)){
        reached = true;
        goal_frame_on = false; // Framing is over
        // Manual turning stop
        base_cmd.angular.z = 0;
        cmd_vel_pub_.publish(base_cmd);
        ROS_INFO("  Framing process succeeded\n\n");
    }
    else {
        // Speed and direction turn determination
        base_cmd.angular.z = (1.5/M_PI) * radiansPI + 0.5;
        if (clockwise) base_cmd.angular.z = -base_cmd.angular.z;
        cmd_vel_pub_.publish(base_cmd);
    }
}
int main(int argc, char **argv)
{
  // Initialize ROS
  ros::init(argc, argv, "Human_Follower");


  HumanFollower* myHumanFollower;
  myHumanFollower = new HumanFollower();

  // Start Spinning
  ros::spin();

  delete myHumanFollower;

  return 0;
}

