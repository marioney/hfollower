#include <hfollower/hpublisher.hpp>

using namespace std;
using namespace std_msgs;

/**
 * A node which gets position from humans detected
 * and choose the optimal one inside the field of
 * view, in accordance with a scoring system.
 * Human position in laser frame
 * It is supposed that once an Human with a determined
 * ID is gone, this ID will not appear any more
 * @author Andrés Alacid Cano
 * @date 2014/03/10
 */

HumanPublisher::HumanPublisher()
{
    // Frame used
    frame = "/laser_top";

    // Weight scoring variables
    k1 = 1;
    k2 = 1.5;
    k3 = 1.1;

    // Scores
    sc1 = 0.0;
    sc2 = 0.0;
    sc3 = 0.0;
    scoreaux = 0.0;

    lastupdateduration = 0.0;
    aux = 0.0;
    detectionDuration = 0.0;
    followingDuration = 0.0;

    // Initialize variables
    idMaxSc = 0;

    // Humans detected subscription callback
    humans_detected_sub = nh.subscribe<hdetect::HumansFeatClass>("/HumansDetected", 10, &HumanPublisher::humans_detectedCallback, this);

    // Objective publisher
    Objective = nh.advertise<geometry_msgs::PoseWithCovarianceStamped>("/HumanObjective", 10);

    ROS_INFO("[HPUBLISHER] Publisher running OK.");
}





// HUMANS DETECTED CALLBACK
void HumanPublisher::humans_detectedCallback(const hdetect::HumansFeatClass HumansDetected)
{
    if (HumansDetected.HumansDetected.size() != 0)
    {
        for (uint i=0 ; i < HumansDetected.HumansDetected.size() ; i++)
        {
            // Detection time scoring. Logarithmic function
            detectionDuration = (ros::Time::now().toSec() - HumansDetected.HumansDetected.at(i).detectiontime);
            //ROS_INFO("Hum Id %d, dectection duration %f",HumansDetected.HumansDetected.at(i).id, detectionDuration);
            sc1 = 36.06 * log(0.1333 * (detectionDuration + 7.50187));

            // Distance scoring. Lineal function
            if((HumansDetected.HumansDetected.at(i).x*HumansDetected.HumansDetected.at(i).x +
                HumansDetected.HumansDetected.at(i).y*HumansDetected.HumansDetected.at(i).y) < 0.3)
            {
              sc2 = 0;
            }
            else
            {
                sc2 = -9.5238*(HumansDetected.HumansDetected.at(i).x*HumansDetected.HumansDetected.at(i).x +
                    HumansDetected.HumansDetected.at(i).y*HumansDetected.HumansDetected.at(i).y) + 114.28;
            }

            // Following time scoring. Logarithmic function
            if (HumansDetected.HumansDetected.at(i).id == HumanObjective.idStruct)
            {
                followingDuration = (ros::Time::now().toNSec() - HumanObjective.followingtimeStruct);
                //ROS_INFO("Hum Id %d, following Duration %f",HumansDetected.HumansDetected.at(i).id, followingDuration);
                sc3 = 36.06 * log(0.1333 * (followingDuration + 7.50187));
            }

            // Total scoring
            scoreaux = k1 * sc1 + k2 * sc2 + k3 * sc3;
            //ROS_INFO("Hum Id: %d, Score: %f", HumansDetected.HumansDetected.at(i).id, scoreaux);

            if (scoreaux > AuxStruct.scoreStruct)
            {
                AuxStruct.idStruct = HumansDetected.HumansDetected.at(i).id;
                AuxStruct.scoreStruct = scoreaux;
             //   ROS_INFO("Best Hum Id: %d, Score: %f", HumansDetected.HumansDetected.at(i).id, scoreaux);
            }
        }

        for(uint i=0 ; i < HumansDetected.HumansDetected.size() ; i++)
        {

            if (HumansDetected.HumansDetected.at(i).id == AuxStruct.idStruct)
            {
                ROS_INFO("Hum to be follower Id: %d, Score: %f", AuxStruct.idStruct, AuxStruct.scoreStruct);
                HumanObjective.xStruct = HumansDetected.HumansDetected.at(i).x;
                HumanObjective.yStruct = HumansDetected.HumansDetected.at(i).y;
                HumanObjective.distanceStruct = (HumansDetected.HumansDetected.at(i).x*HumansDetected.HumansDetected.at(i).x +
                    HumansDetected.HumansDetected.at(i).y*HumansDetected.HumansDetected.at(i).y);
                HumanObjective.detectiontimeStruct = HumansDetected.HumansDetected.at(i).detectiontime;
                HumanObjective.scoreStruct = AuxStruct.scoreStruct;
                if (AuxStruct.idStruct != HumanObjective.idStruct)
                {
                    HumanObjective.followingtimeStruct = ros::Time::now().toSec();
                    HumanObjective.idStruct = AuxStruct.idStruct;
                }
            }
        }

        // Objective publisher
        ObjectiveStamped.header.seq = HumanObjective.idStruct;
        ObjectiveStamped.header.stamp = ros::Time::now();
        ObjectiveStamped.header.frame_id = frame;
        ObjectiveStamped.pose.pose.position.x = HumanObjective.xStruct;
        ObjectiveStamped.pose.pose.position.y = HumanObjective.yStruct;
        Objective.publish(ObjectiveStamped);

        // Reset auxiliar variable
        AuxStruct.idStruct = 0;
        AuxStruct.scoreStruct = -1000; // Guarantee that it is going to be always smaller than new score
    }
}


int main(int argc, char **argv)
{
  // Initialize ROS
  ros::init(argc, argv, "Human_Publisher");


  HumanPublisher* myHumanPublisher;
  myHumanPublisher = new HumanPublisher();

  // Start Spinning
  ros::spin();

  delete myHumanPublisher;

  return 0;
}
