#ifndef HPUBLISHER_HPP
#define HPUBLISHER_HPP

// STANDARD
#include <stdio.h>
#include <string>
#include <sstream>
#include <cmath>

// ROS
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Twist.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_listener.h>

// HDETECT
#include <hdetect/HumansFeatClass.h>



/**
 * A node to catch the position of the closest human
 * to the rostring no nombra a un tipobot and publish it in ROS
 * Human position in the laser frame
 * It is supposed that once an Human with a determined
 * ID is gone, this ID will not appear any more
 * @author Andrés Alacid Cano
 * @date 2014/02/15
 */

class HumanPublisher
{
public:
    HumanPublisher();
    //void scoringprocess();
    //void decisionfollower();
    //void goal_follower();
    //void goal_frame();

    #define ANSI_COLOR_RED   "\x1b[31m"
    #define ANSI_COLOR_RESET   "\x1b[0m"

private:

    ros::NodeHandle nh;

    // Humans detected subscription
    void humans_detectedCallback (const hdetect::HumansFeatClass HumansDetected);
    ros::Subscriber humans_detected_sub;

    // Structs
    struct HumanStruct {
        int idStruct;
        float xStruct;
        float yStruct;
        float distanceStruct;
        double detectiontimeStruct;
        double followingtimeStruct;
        float scoreStruct;
    } HumanObjective;

    struct AuxHumanStruct {
        int idStruct;
        float scoreStruct;
    } AuxStruct;

    // Time variables
    ros::Time lastupdate;
    double lastupdateduration;

    // Scoring variables
    float sc1, sc2, sc3, scoreaux;
    float k1, k2, k3;
    double detectionDuration;
    double followingDuration;
    //ros::Duration detectionDuration;
    //ros::Duration followingDuration;
    double aux;
    int idMaxSc;

    std::string frame;

    // Objective publisher
    ros::Publisher Objective;
    geometry_msgs::PoseWithCovarianceStamped ObjectiveStamped;

};


#endif
