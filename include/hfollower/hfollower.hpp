#ifndef HFOLLOWER_HPP
#define HFOLLOWER_HPP

// STANDARD
#include <stdio.h>
#include <string>
#include <sstream>
#include "cmath"

// ROS
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Twist.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <nav_msgs/Odometry.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/transform_listener.h>

// HDETECT
#include <hdetect/HumansFeatClass.h>



/**
 * A node to catch the position of the closest human
 * to the rostring no nombra a un tipobot and publish it in ROS
 * Human position in the laser frame
 * It is supposed that once an Human with a determined
 * ID is gone, this ID will not appear any more
 * @author Andrés Alacid Cano
 * @date 2014/02/15
 */

class HumanFollower
{
public:
    HumanFollower();
    void scoringprocess();
    void decisionfollower();
    void goal_follower();
    void goal_frame();

    #define ANSI_COLOR_RED   "\x1b[31m"
    #define ANSI_COLOR_RESET   "\x1b[0m"

private:

    ros::NodeHandle nh;

    // Amcl_pose subscription
    void amcl_subCallback(const geometry_msgs::PoseWithCovarianceStamped amcl_robot1_position);
    ros::Subscriber amcl_position;
    float x1amcl, y1amcl, w1amcl, z1amcl, yaw_robotamcl;

    // Odom_combined subscription
    void odom_combinedCallback (const geometry_msgs::PoseWithCovarianceStamped odom_combined_robot);
    ros::Subscriber odom_combined_sub;
    float x1odom, y1odom, w1odom, z1odom, yaw_robotodom;

    // Odom_ subscription
    void odom_Callback (const nav_msgs::Odometry odom_robot);
    ros::Subscriber odom_sub;

    // Humans detected subscription
    void humans_detectedCallback (const hdetect::HumansFeatClass HumansDetected);
    ros::Subscriber humans_detected_sub;

    // Variables
    bool firstData, with_odom_combined, with_odom, with_amcl, firstFollow, reached, goal_sent, goal_frame_on, clockwise;  // General variables
    float xlaser, ylaser, x2, y2, x2rot, y2rot, xgoal, ygoal, xodom, yodom, x2odom, y2odom;  // New objective position and goal position
    float x1, y1, z1, w1, yaw_robot, yaw_first;  // Robot position
    float xdis, ydis, obj_dist, obj_mov, thetaObj;  // Objective position and movement
    float radians, radiansPI; // Angle difference
    int message_counter;  // Message counters

    // Structs
    struct HumanStruct {
        int idStruct;
        float xStruct;
        float yStruct;
        float distanceStruct;
        double detectiontimeStruct;
        double followingtimeStruct;
        float scoreStruct;
    } HumanObjective;

    struct AuxHumanStruct {
        int idStruct;
        float scoreStruct;
    } AuxStruct;

    // Time variables
    ros::Time lastupdate;
    double lastupdateduration;

//    //std::deque<geometry_msgs::PointStamped> HumansDQ;
//    std::deque<Human> HumansIncoming;

    // Scoring variables
    float sc1, sc2, sc3, scoreaux;
    float k1, k2, k3;
    double detectionDuration;
    double followingDuration;
    //ros::Duration detectionDuration;
    //ros::Duration followingDuration;

    double aux;
    int idMaxSc;

    // Goal publication variables
    geometry_msgs::Twist base_cmd;
    ros::Publisher cmd_vel_pub_;
    // Creation of a convenience typedef for a SimpleActionClient to allow us to communicate with actions
    typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
    // Action Client construction
    MoveBaseClient ac;

    std::string frame;

    // Objective publisher
    ros::Publisher Objective;
    geometry_msgs::PoseWithCovarianceStamped ObjectiveStamped;

    /// DEBUGGING
    ros::Publisher obj_position_odom;
    ros::Publisher obj_position_laser;

};


#endif
